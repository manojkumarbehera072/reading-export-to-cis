﻿using System;

namespace TPDDL.ReadingExportToCIS.Models
{
    class ReadRequestResponseModel
    {
        public int ID { get; set; }
        public int RowNumber { get; set; }
        public int CountOfRecords { get; set; }
        public int EquipmentID { get; set; }
        public string DocID { get; set; }
        public string MRReasonCode { get; set; }
        public DateTime MeterReadStartDate { get; set; }
        public DateTime MeterReadEndDate { get; set; }
        public string LogRegno { get; set; }
        public string RegCode { get; set; }
        public string UtilityID { get; set; }
        public string HeaderUUID { get; set; }
        public string ChildUUID { get; set; }
        public string MRResult { get; set; }
        public string MRTime { get; set; }
        public DateTime MRDate { get; set; }
    }
}
