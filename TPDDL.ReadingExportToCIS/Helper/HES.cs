﻿using log4net;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace TPDDL.ReadingExportToCIS.Helper
{
    public class HES
    {

        SqlDataReader reader;
        SqlDataAdapter Adp;
        public static string constr = ConfigurationManager.ConnectionStrings["MDMConnectionString"].ConnectionString;
        public static string version = ConfigurationManager.AppSettings["version"];

        ExportToCISLog exportToCISLog = new ExportToCISLog();

        public DataTable GetSAPAMIUrl(string servicename)
        {
            exportToCISLog.SetLogFileName("Exception_Log");
            log4net.Config.XmlConfigurator.Configure();
            ILog logger = log4net.LogManager.GetLogger("ErrorLog");
            try
            {
                constr = ConfigurationManager.ConnectionStrings["MDMConnectionString"].ConnectionString;
                using (SqlConnection connection = new SqlConnection(constr))
                {
                    connection.Open();
                    string query = "select ServiceUrl,UserId,Password from InterfaceResponseUrls where upper(ServiceName)=upper('" + servicename + "')";
                    SqlCommand cmd = new SqlCommand(query, connection);
                    DataTable dturl = new DataTable();
                    dturl.Clear();
                    Adp = new SqlDataAdapter();
                    Adp.SelectCommand = cmd;
                    Adp.Fill(dturl);

                    connection.Close();
                    return dturl;
                }
            }
            catch (Exception ex)
            {
                string errorLog = "Method :GetSAPAMIUrl --- Version :" + version + " --- Exception :" + ex.Message;
                logger.Error(errorLog);
                throw ex;
            }
        }

    }
}
