﻿using log4net;
using log4net.Repository;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml;
//using System.Web.Script.Serialization;
using System.Xml.Serialization;
using System.Xml.XPath;
using TPDDL.ReadingExportToCIS.Helper;
using TPDDL.ReadingExportToCIS.MeterReadingDocumentERPResultBulkCreateRequest_In;
using TPDDL.ReadingExportToCIS.Models;

namespace TPDDL.ReadingExportToCIS
{
    class Program
    {
        public static string constr = ConfigurationManager.ConnectionStrings["MDMConnectionString"].ConnectionString;
        public static string version = ConfigurationManager.AppSettings["version"];

        static void Main(string[] args)
        {
            string headerUUID = "";
            string readRequestResponseID = "";

            SetLogFileName("Exception_Log");
            log4net.Config.XmlConfigurator.Configure();
            ILog logger = log4net.LogManager.GetLogger("ErrorLog");

            try
            {
                List<ReadRequestResponseModel> readRequestResponseList = new List<ReadRequestResponseModel>();

                ReadRequestResponse readRequestResponse = new ReadRequestResponse();

                readRequestResponseList = readRequestResponse.GetReadRequestResponseByStatus(3);
                var fineRecords = readRequestResponseList.Where(s => s.RowNumber == 1).ToList();

                var duplicateRecords = readRequestResponseList.Where(s => s.RowNumber != 1).ToList();

                List<EquipmentIDAndMREndDate> equipmentIDAndMREndDateList = new List<EquipmentIDAndMREndDate>();
                EquipmentIDAndMREndDate equipmentIDAndMREndDate = new EquipmentIDAndMREndDate();
                HES hes = new HES();

                GetDBName dBName = new GetDBName();
                string dbName = dBName.GetDatabaseName();

                if (fineRecords != null)
                {
                    var filteredList = fineRecords.GroupBy(x => new { x.UtilityID, x.MeterReadEndDate, x.HeaderUUID }).ToList();

                    foreach (var filteredData in filteredList)
                    {

                        var filterdRecords = (List<ReadRequestResponseModel>)fineRecords.Where(o => (o.UtilityID == filteredData.Key.UtilityID) &&
                                (o.MeterReadEndDate == filteredData.Key.MeterReadEndDate) &&
                                (o.HeaderUUID == filteredData.Key.HeaderUUID)).ToList();

                        #region Confirmation
                        MeterReadingDocumentERPResultBulkCreateRequest_In.MeterReadingDocumentERPResultBulkCreateRequest_InBinding cls = new MeterReadingDocumentERPResultBulkCreateRequest_In.MeterReadingDocumentERPResultBulkCreateRequest_InBinding();
                        MeterReadingDocumentERPResultBulkCreateRequest_In.MtrRdngDocERPRsltBulkCrteReqMsg req = new MeterReadingDocumentERPResultBulkCreateRequest_In.MtrRdngDocERPRsltBulkCrteReqMsg();
                        MeterReadingDocumentERPResultBulkCreateRequest_In.BusinessDocumentMessageHeader head = new MeterReadingDocumentERPResultBulkCreateRequest_In.BusinessDocumentMessageHeader();
                        MeterReadingDocumentERPResultBulkCreateRequest_In.UUID uid = new MeterReadingDocumentERPResultBulkCreateRequest_In.UUID();
                        MeterReadingDocumentERPResultBulkCreateRequest_In.UUID REFuid = new MeterReadingDocumentERPResultBulkCreateRequest_In.UUID();

                        MeterReadingDocumentERPResultBulkCreateRequest_In.MtrRdngDocERPRsltCrteReqMsg[] MeterList = new MeterReadingDocumentERPResultBulkCreateRequest_In.MtrRdngDocERPRsltCrteReqMsg[fineRecords.Count];
                        headerUUID = filterdRecords[0].HeaderUUID;
                        uid.Value = headerUUID;
                        System.Guid GUID;
                        GUID = System.Guid.NewGuid();
                        head.ReferenceUUID = uid;
                        REFuid.Value = GUID.ToString();
                        head.UUID = REFuid;
                        head.CreationDateTime = DateTime.Now;
                        head.SenderBusinessSystemID = dbName;
                        DataTable dturl = new DataTable();
                        dturl.Clear();
                        dturl = hes.GetSAPAMIUrl("MeterReading_Response_Bulk");

                        cls.Credentials = new System.Net.NetworkCredential(dturl.Rows[0]["UserId"].ToString(), dturl.Rows[0]["Password"].ToString());
                        cls.Url = dturl.Rows[0]["ServiceUrl"].ToString();

                        req.MessageHeader = head;
                        int i = 0;

                        foreach (var filterdRecord in filterdRecords)//Filtered fineRecords
                        {
                            headerUUID = filterdRecord.HeaderUUID;
                            if (filterdRecord.DocID != null)
                            {
                                readRequestResponseID = filterdRecord.DocID;
                            }
                            else
                            {
                                readRequestResponseID = filterdRecord.ID.ToString();
                            }
                            //Status=4
                            readRequestResponse.UpdateReadRequestResponseByStatusAndID(4, 3, filterdRecord.ID);

                            //Make the conrnfirmation object and send to SAP
                            MeterReadingDocumentERPResultBulkCreateRequest_In.MtrRdngDocERPRsltCrteReqMsg meter = new MeterReadingDocumentERPResultBulkCreateRequest_In.MtrRdngDocERPRsltCrteReqMsg();
                            MeterReadingDocumentERPResultBulkCreateRequest_In.BusinessDocumentMessageHeader childhead = new MeterReadingDocumentERPResultBulkCreateRequest_In.BusinessDocumentMessageHeader();
                            MeterReadingDocumentERPResultBulkCreateRequest_In.MtrRdngDocERPRsltCrteReqMtrRdngDoc mtrRdngDoc = new MeterReadingDocumentERPResultBulkCreateRequest_In.MtrRdngDocERPRsltCrteReqMtrRdngDoc();
                            MeterReadingDocumentERPResultBulkCreateRequest_In.UUID childUuid = new MeterReadingDocumentERPResultBulkCreateRequest_In.UUID();
                            MeterReadingDocumentERPResultBulkCreateRequest_In.UUID childREFuid = new MeterReadingDocumentERPResultBulkCreateRequest_In.UUID();
                            MeterReadingDocumentERPResultBulkCreateRequest_In.MtrRdngDocERPRsltCrteReqRslt result = new MeterReadingDocumentERPResultBulkCreateRequest_In.MtrRdngDocERPRsltCrteReqRslt();
                            MeterReadingDocumentERPResultBulkCreateRequest_In.MeterReadingTypeCode typeCode = new MeterReadingDocumentERPResultBulkCreateRequest_In.MeterReadingTypeCode();
                            MeterReadingDocumentERPResultBulkCreateRequest_In.MeterReadingDocumentID id = new MeterReadingDocumentERPResultBulkCreateRequest_In.MeterReadingDocumentID();
                            MeterReadingDocumentERPResultBulkCreateRequest_In.MtrRdngDocERPRsltCrteReqUtilsMsmtTsk utilitiesMeasurementTask = new MeterReadingDocumentERPResultBulkCreateRequest_In.MtrRdngDocERPRsltCrteReqUtilsMsmtTsk();
                            MeterReadingDocumentERPResultBulkCreateRequest_In.UtilitiesMeasurementTaskID utilitiesMeasurementTaskID = new MeterReadingDocumentERPResultBulkCreateRequest_In.UtilitiesMeasurementTaskID();
                            //MeterReadingDocumentERPResultBulkCreateRequest_In.MtrRdngDocERPRsltCrteReqUtilsPtDeliv utilitiesMeasurementTaskID = new MeterReadingDocumentERPResultBulkCreateRequest_In.UtilitiesMeasurementTaskID();
                            MeterReadingDocumentERPResultBulkCreateRequest_In.MtrRdngDocERPRsltCrteReqUtilsDvce utilsDvce = new MeterReadingDocumentERPResultBulkCreateRequest_In.MtrRdngDocERPRsltCrteReqUtilsDvce();
                            MeterReadingDocumentERPResultBulkCreateRequest_In.UtilitiesDeviceID deviceid = new MeterReadingDocumentERPResultBulkCreateRequest_In.UtilitiesDeviceID();
                            MeterReadingDocumentERPResultBulkCreateRequest_In.MtrRdngDocERPRsltCrteReqMsg mtrRdngDocERPRsltCrteReqMsg = new MeterReadingDocumentERPResultBulkCreateRequest_In.MtrRdngDocERPRsltCrteReqMsg();

                            System.Guid childGUID;
                            childGUID = System.Guid.NewGuid();
                            childUuid.Value = childGUID.ToString();
                            childhead.ReferenceUUID = childREFuid;// childUuid;
                            childREFuid.Value = filterdRecord.ChildUUID.ToString();
                            childhead.UUID = childUuid;
                            childhead.CreationDateTime = DateTime.Now;
                            childhead.SenderBusinessSystemID = dbName;
                            id.Value = readRequestResponseID;
                            mtrRdngDoc.MeterReadingReasonCode = filterdRecord.MRReasonCode;
                            mtrRdngDoc.ScheduledMeterReadingDate = filterdRecord.MeterReadEndDate;
                            utilitiesMeasurementTaskID.Value = filterdRecord.LogRegno;
                            result.ActualMeterReadingDate = filterdRecord.MRDate;
                            result.ActualMeterReadingTime = filterdRecord.MRTime;
                            result.MaximumMeterReadingDate = filterdRecord.MeterReadEndDate;
                            result.MaximumMeterReadingTime = String.IsNullOrEmpty(filterdRecord.MRTime) ? DateTime.MinValue : Convert.ToDateTime(filterdRecord.MRTime);//Need to confirm

                            typeCode.Value = "MD";
                            deviceid.Value = filterdRecord.UtilityID;
                            meter.MessageHeader = childhead;
                            meter.MeterReadingDocument = mtrRdngDoc;
                            meter.MeterReadingDocument.ID = id;
                            //meter.MeterReadingDocument.ScheduledMeterReadingDate = mtrRdngDoc.ScheduledMeterReadingDate;
                            meter.MeterReadingDocument.Result = result;
                            meter.MeterReadingDocument.Result.MeterReadingResultValue = String.IsNullOrEmpty(filterdRecord.MRResult) ? 0 : Convert.ToDecimal(filterdRecord.MRResult);//Need to confirm
                            meter.MeterReadingDocument.Result.MeterReadingTypeCode = typeCode;
                            meter.MeterReadingDocument.UtiltiesMeasurementTask = utilitiesMeasurementTask;
                            meter.MeterReadingDocument.UtiltiesMeasurementTask.UtilitiesObjectIdentificationSystemCodeText = filterdRecord.RegCode;
                            meter.MeterReadingDocument.UtiltiesMeasurementTask.UtilitiesMeasurementTaskID = utilitiesMeasurementTaskID;
                            meter.MeterReadingDocument.UtiltiesMeasurementTask.UtiltiesDevice = utilsDvce;
                            meter.MeterReadingDocument.UtiltiesMeasurementTask.UtiltiesDevice.UtilitiesDeviceID = deviceid;
                            MeterList[i] = meter;

                            i++;
                        }
                        req.MeterReadingDocumentERPResultCreateRequestMessage = MeterList;

                        cls.MeterReadingDocumentERPResultBulkCreateRequest_In(req);

                        #endregion
                        //var stringwriter = new System.IO.StringWriter();
                        //var serializer = new XmlSerializer(typeof(MtrRdngDocERPRsltBulkCrteReqMsg));
                        //serializer.Serialize(stringwriter, req);
                        //string xml = stringwriter.ToString();

                        var xmlString = Serialize<MtrRdngDocERPRsltBulkCrteReqMsg>(req);
                        WriteLog("ReadingExportToCIS_" + DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss"), xmlString);
                    }
                }

                if (duplicateRecords != null)
                {
                    foreach (var duplicateRecord in duplicateRecords)
                    {
                        //Status=7
                        readRequestResponse.UpdateReadRequestResponseByStatusAndID(7, 3, duplicateRecord.ID);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorLog = "Method :ReadingExportToCIS --- Version :" + version + " --- Exception :" + ex.Message + " ";
                logger.Error(errorLog);
            }
            
        }

        private static string Serialize<T>(T dataToSerialize)
        {
            try
            {
                var stringwriter = new System.IO.StringWriter();
                var serializer = new XmlSerializer(typeof(T));
                serializer.Serialize(stringwriter, dataToSerialize);
                return stringwriter.ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }        
        private static void WriteLog(string strFileName, string strMessage)
        {
            try
            {
                string payloadPath = ConfigurationManager.AppSettings["logPath"];
                FileStream objFilestream = new FileStream(string.Format("{0}\\{1}", payloadPath, strFileName + ".txt"), FileMode.Append, FileAccess.Write);
                StreamWriter objStreamWriter = new StreamWriter((Stream)objFilestream);
                objStreamWriter.WriteLine(strMessage);
                objStreamWriter.Close();
                objFilestream.Close();
            }
            catch (Exception)
            {
                throw;
            }
        }
        private static void SetLogFileName(string name)
        {
            log4net.Repository.ILoggerRepository RootRep;
            RootRep = LogManager.GetRepository(Assembly.GetCallingAssembly());

            XmlElement section = ConfigurationManager.GetSection("log4net") as XmlElement;

            XPathNavigator navigator = section.CreateNavigator();
            XPathNodeIterator nodes = navigator.Select("appender/file");
            foreach (XPathNavigator appender in nodes)
            {
                appender.MoveToAttribute("value", string.Empty);
                appender.SetValue(string.Format(appender.Value, name + "+"));
            }

            IXmlRepositoryConfigurator xmlCon = RootRep as IXmlRepositoryConfigurator;
            xmlCon.Configure(section);
        }

    }
}

