﻿using log4net;
using System;
using System.Configuration;
using System.Data.SqlClient;

namespace TPDDL.ReadingExportToCIS.Helper
{
    public class GetDBName
    {
        SqlDataReader reader;
        public static string constr = ConfigurationManager.ConnectionStrings["MDMConnectionString"].ConnectionString;
        public static string version = ConfigurationManager.AppSettings["version"];

        string dbName = "";
        ExportToCISLog exportToCISLog = new ExportToCISLog();

        public string GetDatabaseName()
        {
            exportToCISLog.SetLogFileName("Exception_Log");
            log4net.Config.XmlConfigurator.Configure();
            ILog logger = log4net.LogManager.GetLogger("ErrorLog");

            try
            {
                constr = ConfigurationManager.ConnectionStrings["MDMConnectionString"].ConnectionString;
                using (SqlConnection connection = new SqlConnection(constr))
                {
                    connection.Open();
                    string query = "select  DB_NAME() as DataBaseName";
                    SqlCommand cmd = new SqlCommand(query, connection);
                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        dbName = reader["DataBaseName"].ToString();
                    }
                    connection.Close();
                    return dbName;
                }
            }
            catch (Exception ex)
            {
                string errorLog = "Method :GetDatabaseName --- Version :" + version + " --- Exception :" + ex.Message;
                logger.Error(errorLog);
                throw ex;
            }
        }
    }
}

