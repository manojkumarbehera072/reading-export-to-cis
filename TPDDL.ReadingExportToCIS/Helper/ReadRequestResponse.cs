﻿using log4net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using TPDDL.ReadingExportToCIS.Models;

namespace TPDDL.ReadingExportToCIS.Helper
{
    class ReadRequestResponse
    {
        public static string constr = ConfigurationManager.ConnectionStrings["MDMConnectionString"].ConnectionString;
        public static string version = ConfigurationManager.AppSettings["version"];

        ExportToCISLog exportToCISLog = new ExportToCISLog();

        public List<ReadRequestResponseModel> GetReadRequestResponseByStatus(int status)
        {
            exportToCISLog.SetLogFileName("Exception_Log");
            log4net.Config.XmlConfigurator.Configure();
            ILog logger = log4net.LogManager.GetLogger("ErrorLog");

            try
            {
                List<ReadRequestResponseModel> readRequestResponseModels = new List<ReadRequestResponseModel>();
                using (SqlConnection connection = new SqlConnection(constr))
                {
                    connection.Open();
                    string query = "Select ROW_NUMBER() OVER(PARTITION BY A.Equipment_ID,A.MeterReadEndDate,A.RegCode ORDER BY A.[Created_on] DESC) AS 'RowNumber'," +
                        "COUNT(1) OVER(PARTITION BY A.Equipment_ID,A.MeterReadEndDate,A.RegCode) AS CountOfRecords,A.* from (Select * from [dbo].[ReadRequestResponse] where Status='" + status + "' AND RecordStatus='R') A";

                    SqlCommand cmd = new SqlCommand(query, connection);
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        ReadRequestResponseModel readRequestResponseModel = new ReadRequestResponseModel();
                        readRequestResponseModel = new ReadRequestResponseModel();
                        readRequestResponseModel.ID = reader["ID"]==DBNull.Value ? 0:Convert.ToInt32(reader["ID"].ToString()); 
                        readRequestResponseModel.RowNumber = reader["RowNumber"] == DBNull.Value ? 0:Convert.ToInt32(reader["RowNumber"].ToString()) ;
                        readRequestResponseModel.CountOfRecords = reader["CountOfRecords"] == DBNull.Value ? 0: Convert.ToInt32(reader["CountOfRecords"].ToString()) ;
                        readRequestResponseModel.EquipmentID = reader["Equipment_ID"]==DBNull.Value ? 0:Convert.ToInt32(reader["Equipment_ID"].ToString()) ;
                        readRequestResponseModel.DocID = reader["Doc_ID"].ToString();
                        readRequestResponseModel.MRReasonCode = reader["MRReasonCode"].ToString();
                        readRequestResponseModel.MeterReadStartDate = reader["MeterReadStartDate"] == DBNull.Value ? DateTime.MinValue: Convert.ToDateTime(reader["MeterReadStartDate"]);
                        readRequestResponseModel.MeterReadEndDate = reader["MeterReadEndDate"] == DBNull.Value ? DateTime.MinValue:Convert.ToDateTime(reader["MeterReadEndDate"]);
                        readRequestResponseModel.LogRegno = reader["LogRegno"].ToString();
                        readRequestResponseModel.RegCode = reader["RegCode"].ToString();
                        readRequestResponseModel.UtilityID = reader["Utility_ID"].ToString();
                        readRequestResponseModel.HeaderUUID = reader["HeaderUUID"].ToString();
                        readRequestResponseModel.ChildUUID = reader["ChildUUID"].ToString();
                        readRequestResponseModel.MRResult = reader["MRResult"].ToString();
                        readRequestResponseModel.MRDate = reader["MRDate"] == DBNull.Value ? DateTime.MinValue : Convert.ToDateTime(reader["MRDate"]);
                        readRequestResponseModel.MRTime = reader["MRTime"].ToString();
                        readRequestResponseModels.Add(readRequestResponseModel);
                    }
                    connection.Close();
                    return readRequestResponseModels;
                }
            }
            catch (Exception ex)
            {
                string errorLog = "Method :GetSAPAMIUrl --- Version :"+ version + "--- Exception :" + ex.Message;
                logger.Error(errorLog);
                throw ex;
            }
        }

        public void UpdateReadRequestResponseByStatusAndID(int status,int oldStatus,int id)
        {
            string recordStatus = "R";
            if (status == 4 && oldStatus == 3)
            {
                recordStatus = "S";
            }

            using (SqlConnection connection = new SqlConnection(constr))
            {
                connection.Open();
                string query = "update ReadRequestResponse set Status='" + status + "',RecordStatus='" + recordStatus + "',Changed_on = '" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + "' where ID='" + id + "' and Status='" + oldStatus + "'";
                SqlCommand cmd = new SqlCommand(query, connection);
                cmd.ExecuteNonQuery();
                connection.Close();
            }
        }
    }
}
