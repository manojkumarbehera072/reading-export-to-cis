﻿using System;

namespace TPDDL.ReadingExportToCIS.Models
{
    class EquipmentIDAndMREndDate
    {
        public int  EquipmentId { get; set; }
        public DateTime MREndDate { get; set; }
    }
}
